// Server.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#define _WIN32_WINNT 0x501
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

using namespace std;

void main()
{
	//Инициализация winsock
	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2);

	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0) {
		cerr << "Can't initialize winock!" << endl;
		return;
	}

	//Создание сокета
	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == INVALID_SOCKET) {
		cerr << "Can't create a socket!" << endl;
		return;
	}
	//Связать сокет с ip и port
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(54000);
	hint.sin_addr.S_un.S_addr = INADDR_ANY;

	bind(listening, (sockaddr*)&hint, sizeof(hint));

	// Слушаем winsock
	listen(listening, SOMAXCONN);

	// Ждем коннекта
	sockaddr_in client;
	int clientSize = sizeof(client);

	SOCKET clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
	if (clientSocket == INVALID_SOCKET) {
		cerr << "Don't accept" << endl;
	}

	char host[NI_MAXHOST]; // удаленное имя клиента
	char service[NI_MAXHOST]; // Клиент законнектился

	memset(host, 0, NI_MAXHOST);

	if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		cout << host << " connected on port " << service << endl;
	}
	else
	{
		inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
		cout << host << "connected on port" <<
			ntohs(client.sin_port) << endl;
	}
		
	//Закрытие слушающего сокета
	closesocket(listening);

	//принять и прослушать сообщение и отправить клиенту
	char buf[4096];

	while (true) {
		ZeroMemory(buf, 4096);

		//Ждем отправки данных от клиента
		int bytesReceived = recv(clientSocket, buf, 4096, 0);
		if (bytesReceived == SOCKET_ERROR) 
		{
			cerr << "Error in recv(). " << endl;
			break;			
		}

		if (bytesReceived == 0) {
			cout << " Client disconnected" << endl;
			break;
		}
		//Отправить сообщение обратно клиенту
		send(clientSocket, buf, bytesReceived + 1, 0);
	}

	//Закрытие сокета
	closesocket(clientSocket);

	//Выключить windsock
	WSACleanup();




}
