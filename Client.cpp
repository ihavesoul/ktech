// Client.cpp: определяет точку входа для консольного приложения.
#include "stdafx.h"
#include <iostream>
#include <WS2tcpip.h>
#include <string>
#pragma comment (lib,"ws2_32.lib")

using namespace std;

void main()
{
	string ipAddress = "127.0.0.1"; 
	int port = 54000;

	//Инициализация winsock
	WSADATA data;
	WORD ver = MAKEWORD(2, 2);
	int wsResult = WSAStartup(ver, &data);
	if (wsResult != 0) {
		cerr << "Can't start Winsock , Error # " << wsResult << endl;
		return;
	}
	
	//Создать сокет
	SOCKET sock = socket(AF_INET , SOCK_STREAM, 0 );
	if (sock == INVALID_SOCKET) {
		cerr << "Can't create socket ,Err #" << WSAGetLastError() << endl;
	}
	//
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(port);
	inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);

	//Коннект с сервером
	int connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
	if (connResult == SOCKET_ERROR) {
		cerr << "Can't connect to server, Err #" << WSAGetLastError() << endl;
		WSACleanup();
		return;
	}
	//Отправка и принятие данных
	char buff[4096];
	string userInput;

	do {
		// Ввод текста юзером
		cout << "> ";
		getline(cin, userInput);

		if (userInput.size() > 0) 
		{
			// Отправка текста
			int sendResult = send(sock, userInput.c_str(), userInput.size() + 1, 0);
			if (sendResult != SOCKET_ERROR) 
			{
				// Ожидание ответа
				ZeroMemory(buff, 4096);
				int bytesReceived = recv(sock, buff, 4096, 0);
				// Эхо запрос в консоль
				if (bytesReceived > 0) {
					cout << "SERVER>" << string(buff, 0, bytesReceived) << endl;
				}
			}
		}
	} while (userInput.size() > 0);
	//Закрыть все
	closesocket(sock);
	WSACleanup();
	

}

